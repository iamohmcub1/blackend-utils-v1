#!/bin/sh
# This script is use to build and run slot gen 2 server on local docker

NETWORK_NAME="ohm"
IMAGE_NAME="blackend-utils-v1"
CONTAINER_NAME="blackend-utils-v1"

CUSTOM_SERVICE_PORT="PORT=18888"
CUSTOM_CONTAINER_PORT="18888:18888"

TAG=$(date +"%Y%m%d-%H%M")
FULL_IMAGE_NAME="${IMAGE_NAME}:${TAG}"

npm run build
docker build -t $FULL_IMAGE_NAME .
docker run -d --net $NETWORK_NAME --name $CONTAINER_NAME-$TAG -e $CUSTOM_SERVICE_PORT -p $CUSTOM_CONTAINER_PORT $FULL_IMAGE_NAME
$SHELL