import { EUserRole, IRequestBodyGrantBOUser, agentRole, spxRole } from "./constants";
import { Request } from "express";
import * as bcrypt from "bcryptjs";
import mongoose from "mongoose";

export default class BackofficeUser {
    constructor() { }
    public static async requestCreateUser(req: Request<{}, {}, IRequestBodyGrantBOUser>, res: any, next: any) {
        const requestBody = req.body as IRequestBodyGrantBOUser;
        const result: Record<string, any> = [];
        const inputCount = requestBody.list.length;
        const noSuccessIndex = [];
        let index = 0;
        for (let agent of requestBody.list) {
            const mongoObjectId = new mongoose.Types.ObjectId();
            if (spxRole.includes(agent.role) == true) {
                if (agent.username == null || agent.password == null || typeof agent.username != 'string' || typeof agent.username != 'string') {
                    noSuccessIndex.push(index);
                } else {
                    result.push({
                        _id: { "$oid": `${mongoObjectId}` },
                        user_id: mongoObjectId.toString(),
                        display_name: agent.username,
                        type: agent.role,
                        auth: {
                            username: agent.username,
                            password: bcrypt.hashSync(agent.password, 10),
                            role: agent.role
                        },
                        created_by: EUserRole.ADMIN,
                        created_timestamp: {
                            "$date": new Date()
                        },
                        updated_timestamp: {
                            "$date": new Date()
                        }
                    })
                }
            }
            if (agentRole.includes(agent.role) == true) {
                if (agent.id == null || agent.username == null || agent.password == null || typeof agent.id != 'string' || typeof agent.username != 'string' || typeof agent.username != 'string') {
                    noSuccessIndex.push(index);
                } else {
                    result.push({
                        _id: { "$oid": `${mongoObjectId}` },
                        user_id: mongoObjectId.toString(),
                        agent_id: agent.id,
                        display_name: agent.username,
                        type: EUserRole.AGENT,
                        auth: {
                            username: agent.username,
                            password: bcrypt.hashSync(agent.password, 10),
                            role: agent.role
                        },
                        created_by: EUserRole.ADMIN,
                        created_timestamp: {
                            "$date": new Date()
                        },
                        updated_timestamp: {
                            "$date": new Date()
                        }
                    })
                }
            }
            if (agent.role == null || (agentRole.includes(agent.role) == false && spxRole.includes(agent.role) == false)) {
                noSuccessIndex.push(index);
            }
            index++;
        }
        res.json({
            input_count: inputCount,
            no_success_count: noSuccessIndex.length,
            no_success_index: noSuccessIndex,
            result: result,
        })
    }
}