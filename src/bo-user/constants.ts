export enum EUserRole {
    ADMIN = 'admin',
    MASTER_SUP = 'master_support',
    SUPPORT = 'support',

    ROOT_MASTER_AGENT = 'root_master_agent',
    MASTER_AGENT = 'master_agent',
    AGENT = 'agent'
}

export const spxRole: string[] = [EUserRole.ADMIN, EUserRole.MASTER_SUP, EUserRole.SUPPORT];
export const agentRole: string[] = [EUserRole.ROOT_MASTER_AGENT, EUserRole.MASTER_AGENT, EUserRole.AGENT];

interface IUerList {
    id: string,
    username: string,
    password: string,
    role: string,
}

export interface IRequestBodyGrantBOUser {
    list: IUerList[]
}