import MD5 from "crypto-js/md5"
import { Request } from "express";
import { IRequestBodyCheckSignature } from "./constants";

interface IObject {
    [key: string]: any
}
export default class SignatureService {
    constructor() { }
    public static async requestCheckSignature(req: Request<{}, {}, IRequestBodyCheckSignature>, res: any, next: any) {
        const requestBody = req.body;
        let inputKey = req.headers["sig"] as string;
        if (inputKey == null) {
            res.json({
                message: "invalid input"
            })
        }
        else {
            const before = SignatureService.sign(requestBody, inputKey);
            const md5 = SignatureService.hashValue(before);
            res.json({
                body: requestBody,
                signature: inputKey,
                before: before,
                md5: md5
            })
        }
    }

    public static sign(requestBody: IObject, signatureKey: string) {
        let value = `${signatureKey}`;
        let keyObject = SignatureService._getKeyValueFromObject(requestBody);
        let keys = Object.keys(keyObject);
        let sortedKeys = keys.sort();
        for (let key of sortedKeys) {
            value += `&${key}=${keyObject[key]}`;
        }
        return value;
    }

    public static hashValue(value: string) {
        return MD5(value).toString();
    }

    private static _getKeyValueFromObject(args: IObject, prefixKey: string = "") {
        let result: IObject = {};
        for (let key in args) {
            if (args[key] == null || typeof args[key] == "undefined") {
                continue;
            }
            if (typeof args[key] != "object") {
                let resultKey = "";
                if (prefixKey != "") {
                    resultKey = `${prefixKey}.`;
                }
                resultKey += key;
                result[resultKey] = args[key];
                continue;
            }
            if (Object.keys(args[key]).length > 0) {
                let nestedPrefixKey = key;
                if (prefixKey != "") {
                    nestedPrefixKey = `${prefixKey}.${key}`;
                }
                let nestedResult = SignatureService._getKeyValueFromObject(args[key], nestedPrefixKey);
                result = Object.assign(result, nestedResult);
            }
        }
        return result;
    }
}

// const signature = SignatureService.sign(
//     {
//         "platform_id": "63d8821fb09a54d5295eca06"
//     },
//     "61ed7eed705e2ae12c9e05760a077141"
// );
// console.log(signature);
