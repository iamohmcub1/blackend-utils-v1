import http from "http";
import express from "express";
import GeneralRoutes from "./router";

const app = express();
const server = http.createServer(app);
server.listen(18888, () => {
    console.log("server is running on port 18888");
    app.use(express.json());
    app.use('', GeneralRoutes);
});