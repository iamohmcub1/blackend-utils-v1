import express from 'express';
import SignatureService from './signature/signature';
import BackofficeUser from './bo-user/logic';
import CheckerController from './checkers';

const router = express.Router();

router.get('/check', (req, res, next) => {
    return res.status(200).json({ message: "Ok" });
});

router.post('/checkSignature',
    CheckerController.checkParam_requestSignature,
    SignatureService.requestCheckSignature
);

router.post('/boUser',
    CheckerController.checkParam_requestBoUser,
    BackofficeUser.requestCreateUser
);

export default router;