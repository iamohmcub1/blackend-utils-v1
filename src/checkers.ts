import * as Joi from "joi";

export default class CheckerController {
    private static _checkerSignature = Joi.object({}).unknown(true).required();
    private static _boSignature = Joi.object({
        list: Joi.array().items({
            id: Joi.string(),
            username: Joi.string().required(),
            password: Joi.string().required(),
            role: Joi.string().required()
        })
    });

    private static _schemaMethod: { [key: string]: Joi.ObjectSchema } = {
        signature: this._checkerSignature,
        boUser: this._boSignature,

    }
    public static validateParams(requestKey: string, requestParam: Request["body"]) {
        if (CheckerController._schemaMethod[requestKey]) {
            let validate = this._schemaMethod[requestKey].validate(requestParam, { convert: false, abortEarly: false });
            if (validate.error != null) {
                throw validate.error.message;
            }
        } else {
            throw "Invalid validation schema"
        }
    };

    private static _checkParam(req: any, res: any, next: any, requestKey: string) {
        try {
            CheckerController.validateParams(requestKey, req.body);
            next();
        } catch (e) {
            return res.status(500).json({
                message: e
            });
        }
    }

    public static checkParam_requestSignature(req: any, res: any, next: any) {
        CheckerController._checkParam(req, res, next, "signature");
    };
    public static checkParam_requestBoUser(req: any, res: any, next: any) {
        CheckerController._checkParam(req, res, next, "boUser");
    };

}