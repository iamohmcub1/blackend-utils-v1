FROM node:12-alpine
COPY package.json /app/package.json
COPY . /app

RUN cd /app && npm install

WORKDIR /app

CMD ["npm",  "start"]

EXPOSE 3000
